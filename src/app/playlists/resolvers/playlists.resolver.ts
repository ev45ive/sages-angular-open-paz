import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { first, startWith, take } from 'rxjs/operators';
import { Playlists } from 'src/app/core/actions/playlist.actions';
import { MusicApiService } from 'src/app/core/services/music-api.service';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { PlaylistsState } from 'src/app/core/store/playlists.state';
import { Playlist } from '../model/Playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsResolver implements Resolve<Playlist[]> {

  constructor(private service: PlaylistsService,
    private store: Store
  ) { }


  @Select(PlaylistsState.playlists) playlists!: Observable<Playlist[]>;

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Playlist[]> {

    // this.service.getUserPlaylists()

    this.store.dispatch(new Playlists.FetchAll()).subscribe()

    // return this.service.playlistsChanges.pipe(take(1)) // must complete to load component
    return this.playlists.pipe(first())
  }
}
