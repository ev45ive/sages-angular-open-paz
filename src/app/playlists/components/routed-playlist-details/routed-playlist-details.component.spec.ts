import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { playlistsMocks } from 'src/app/core/services/playlistsMocks';
import { YesnoPipe } from 'src/app/shared/pipes/yesno.pipe';

import { RoutedPlaylistDetailsComponent } from './routed-playlist-details.component';

fdescribe('RoutedPlaylistDetailsComponent', () => {
  let component: RoutedPlaylistDetailsComponent;
  let fixture: ComponentFixture<RoutedPlaylistDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RoutedPlaylistDetailsComponent, YesnoPipe],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: jasmine.createSpyObj<ActivatedRoute>('ActivatedRoute', ['data'])
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    const route = TestBed.inject(ActivatedRoute) as jasmine.SpyObj<ActivatedRoute>
    route.data = of({ playlist: playlistsMocks[0] })
    
    fixture = TestBed.createComponent(RoutedPlaylistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
