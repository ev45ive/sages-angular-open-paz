import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { playlistsMocks } from 'src/app/core/services/playlistsMocks';
import { CardComponent } from 'src/app/shared/components/card/card.component';
import { TrackListComponent } from 'src/app/shared/components/track-list/track-list.component';
import { YesnoPipe } from 'src/app/shared/pipes/yesno.pipe';

import { PlaylistDetailsComponent } from './playlist-details.component';

describe('PlaylistDetailsComponent', () => {

  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;
  let elem: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent, YesnoPipe, CardComponent, TrackListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent);
    component = fixture.componentInstance;
    elem = fixture.debugElement
    component.playlist = playlistsMocks[0]
    fixture.detectChanges();

  });
  it('should create', () => { expect(component).toBeTruthy(); });
  it('should show playlist details', () => {
    expect(elem.query(By.css('.card-title')).nativeElement.textContent).toMatch(playlistsMocks[0].name)
    expect(elem.query(By.css(playlistsMocks[0].public ? '.text-danger' : '.text-success'))).not.toBeNull()
  });

  
  it('should emit "edit" event when edit button clicked ', () => {
    const spy = jasmine.createSpy('Edit event')
    component.edit.subscribe(spy)

    elem.query(By.css('button')).triggerEventHandler('click', {})

    expect(spy).toHaveBeenCalledTimes(1)
  })
});
