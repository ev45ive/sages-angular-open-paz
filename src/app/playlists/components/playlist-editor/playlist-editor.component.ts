import { AfterViewInit, Component, DoCheck, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent {
  @Input() playlist: Playlist = {
    id: '',
    name: '',
    public: false,
    description: ''
  }
  extras = false

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (changes['playlist']) {
      this.formRef?.resetForm()
    }
  }

  hasUnsavedChanges() {
    return true
  }


  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  constructor() { }

  @ViewChild('nameRef', { static: true, read: ElementRef })
  nameRef?: ElementRef<HTMLInputElement>

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // console.log('ngAfterViewInit');
    // this.nameRef?.nativeElement.focus()
    // this.formRef?.setValue({})
  }

  @ViewChild('formRef', { read: NgForm })
  formRef?: NgForm

  submit() {
    if (this.formRef?.invalid) { return }

    // this.formRef?.controls['name'].setValidators([])
    // this.formRef?.controls['name'].valid
    // this.formRef?.valid

    const draft: Playlist = {
      ...this.playlist,
      ...this.formRef?.value
    }

    this.save.emit(draft)
  }

  cancelClick() {
    this.cancel.emit()
  }

}
