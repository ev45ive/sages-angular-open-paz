import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { filter, map, startWith, switchMap } from 'rxjs/operators';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { PlaylistEditorComponent } from '../../components/playlist-editor/playlist-editor.component';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {
  message = ''
  mode: 'details' | 'edit' | 'create' = 'details'

  playlists = this.service.playlistsChanges

  selectedId = this.route.queryParamMap.pipe(
    map(pm => pm.get('id')),
    filter(id => !!id)
  )

  // forceUpdate = new BehaviorSubject(1)
  // selected = combineLatest([this.selectedId, this.forceUpdate]).pipe(
  //   map(([id]) => id), switchMap(id => this.service.getPlaylistById(id!)))

  selected = this.selectedId.pipe(
    switchMap(id => this.service.getPlaylistById(id!))
  )

  constructor(
    private service: PlaylistsService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  createMode() {
    this.mode = 'create'
  }

  @ViewChild(PlaylistEditorComponent)
  editor?: PlaylistEditorComponent

  select(id: Playlist['id']) {
    this.router.navigate(['/playlists'], { queryParams: { id } })
  }

  ngOnInit(): void {
    this.service.getUserPlaylists()
  }

  editMode() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'details'
  }


  savePlaylist(draft: Playlist) {
    this.service.updatePlaylist(draft)
      .subscribe(() => this.mode = 'details')
  }


  saveNewPlaylist(draft: Playlist) {
    this.service.createPlaylist(draft)
      .subscribe((draft) => {
        this.select(draft.id)
        this.mode = 'details'
      })
  }

  removePlaylist(id: Playlist['id']) {
    this.service.deletePlaylist(id)
      .subscribe(() => this.mode = 'details')
  }
}
