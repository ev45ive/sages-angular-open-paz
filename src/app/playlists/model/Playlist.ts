import { PagingObject, Track } from "src/app/core/model/album";

export interface Playlist {
    id: string;
    name: string;
    public: boolean;
    description: string;
    tracks?:PagingObject<PlaylistTrackObject>
}

interface PlaylistTrackObject{
    track: Track
}