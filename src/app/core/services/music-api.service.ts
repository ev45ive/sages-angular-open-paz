import { Inject, Injectable } from '@angular/core';
import { Album, AlbumsSearchResponse, PagingObject } from '../model/album';
import { API_URL } from './tokens';
import { HttpClient } from '@angular/common/http'

import { map } from 'rxjs/operators'
import { Playlist } from 'src/app/playlists/model/Playlist';
@Injectable({
  providedIn: 'root',
})
export class MusicApiService {

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private api_url: string,
    // @Inject(INITIAL_ALBUMS_SEARCH) private results: SimplifiedAlbumObject[],
  ) { }

  searchAlbums(query: string) {
    return this.http.get<AlbumsSearchResponse>(`${this.api_url}/search`, {
      params: { type: 'album', q: query },
    }).pipe(map(resp => resp.albums.items))
  }

  getAlbumById(id: string) {
    return this.http.get<Album>(`${this.api_url}/albums/${id}`)
  }

  getCurrentUserPlaylists() {
    return this.http.get<PagingObject<Playlist>>(`${this.api_url}/me/playlists`)
      .pipe(map(p => p.items))
  }

  getPlaylistById(id: string) {
    return this.http.get<Playlist>(`${this.api_url}/playlists/${id}`)
  }

  removePlaylistById(playlist_id: string) {
    return this.http.delete<Playlist>(`${this.api_url}/playlists/${playlist_id}/followers`)
  }

  createPlaylist(user_id: string, draft: Playlist) {
    return this.http.post<Playlist>(`${this.api_url}/users/${user_id}/playlists`, {
      name: draft.name,
      public: draft.public,
      description: draft.description,
    })
  }

  updatePlaylist(draft: Playlist) {
    return this.http.put<Playlist>(`${this.api_url}/playlists/${draft.id}`, {
      name: draft.name,
      public: draft.public,
      description: draft.description,
    })
  }
}
