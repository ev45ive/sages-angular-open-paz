import { Inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { API_URL, INITIAL_ALBUMS_SEARCH } from './services/tokens';
import { OAuthModule } from 'angular-oauth2-oidc'
import { AuthService } from './services/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { NgxsModule } from '@ngxs/store';
import { PlaylistsState } from './store/playlists.state';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true,
        allowedUrls: [environment.api_url]
      }
    }),
    NgxsModule.forRoot([
      PlaylistsState
    ], { developmentMode: !environment.production })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    {
      provide: INITIAL_ALBUMS_SEARCH,
      useValue: []
    },
    // {
    //   provide: MusicApiService,
    //   // useValue: new MusicApiService()
    //   useFactory(api_url: string, results: SimplifiedAlbumObject[]) {
    //     return new MusicApiService(api_url, results)
    //   },
    //   deps: [API_URL, INITIAL_ALBUMS_SEARCH]
    // },
    // {
    //   provide: AbstractMusicApiService,
    //   useClass: SpotifyMusicApiService,
    //   // deps: [API_URL, INITIAL_ALBUMS_SEARCH] // extract from @Injectable/@Inject
    // },
    // MusicApiService,
  ]
})
export class CoreModule {
  constructor(
    // @Inject(HTTP_INTERCEPTORS) interceptors: any,
    // @Inject(ROUTES) routes: any,
    private auth: AuthService) {


    this.auth.init()

  }
}
