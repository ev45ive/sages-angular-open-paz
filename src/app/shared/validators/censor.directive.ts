import { Directive, Input } from '@angular/core';
import { AbstractControl, NgForm, NgModel, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appCensor]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CensorDirective,
      multi: true,
    }
  ]
})
export class CensorDirective implements Validator {

  @Input('appCensor') badword: string = ''

  constructor(
    // private model?: NgModel
    // private form?: NgForm
  ) { }

  validate(control: AbstractControl): ValidationErrors | null {

    const hasBadWord = String(control.value).includes(this.badword)

    return hasBadWord ? {
      // 'required': true,
      // 'minlength': { requiredLength: 'placki' },
      'censor': { badword: this.badword }
    } : null

  }

}
