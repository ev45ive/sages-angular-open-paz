import { ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // ignore parent updates
})
export class ClockComponent implements OnInit {
  // public cdr
  time = ''

  constructor(
    public cdr: ChangeDetectorRef,
    public zone: NgZone) {
    // cdr.detach()
    this.time = (new Date()).toLocaleTimeString()
  }

  ngOnInit(): void {
    this.zone.runOutsideAngular(() => { // dont trigger parent

      setInterval(() => {
        this.time = (new Date()).toLocaleTimeString()
        this.cdr.detectChanges()
      }, 1000)

    })
  }

}
