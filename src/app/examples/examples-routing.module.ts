import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DropdownsComponent } from './dropdowns/dropdowns.component';

const routes: Routes = [
  {
    path: 'examples',
    children: [
      {
        path: '', pathMatch: 'full', redirectTo: 'dropdowns'
      },
      {
        path: 'dropdowns',
        component: DropdownsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamplesRoutingModule { }
