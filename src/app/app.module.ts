import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { ExamplesModule } from './examples/examples.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { INITIAL_ALBUMS_SEARCH } from './core/services/tokens';
import { mockAlbums } from './mockAlbums';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlaylistsModule,
    ExamplesModule,
    SharedModule,
    CoreModule,
    /// MocksModule
  ],
  providers: [
    {
      provide: INITIAL_ALBUMS_SEARCH,
      useValue: mockAlbums
    },
  ],
  bootstrap: [AppComponent/* HeaderComponent, SidebarComponent */]
})
export class AppModule { }

// export class AppModule implements DoBootstrap {
//   ngDoBootstrap(app: ApplicationRef) {
//     app.bootstrap(AppComponent, 'app-root')
//     app.bootstrap(AppComponent, '.placki')
//   }
// }
