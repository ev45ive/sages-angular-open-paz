# GIT

cd ..
git clone https://bitbucket.org/ev45ive/sages-angular-open-paz.git sages-angular-open-paz
cd sages-angular-open-paz
npm install
npm start

## Git Update

git stash -u
git pull

## Instalacje

node -v
v14.17.0

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.60.1
83bd43bc519d15e50c4272c6cf5c1479df196a4d
x64

chrome://version/
Google Chrome 93.0.

## Extensions

https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh

## Angular CLI

npm install -g @angular/cli
C:\Users\PC\AppData\Roaming\npm\ng -> C:\Users\PC\AppData\Roaming\npm\node_modules\@angular\cli\bin\ng

PATH - C:\Users\PC\AppData\Roaming\npm\ng

ng --version
Angular CLI: 12.2.9
Node: 14.17.0
Package Manager: npm 6.14.6
OS: win32 x64

ng.cmd --version

## Create new project

ng new angular-open --directory . --strict
ng new angular-open --directory . --strict --routing --style scss

## Start dev server

npm start

<!-- or -->

ng serve

<!-- or -->

ng s -o

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **

## Generators

ng g c clock
CREATE src/app/clock/clock.component.html (20 bytes)
CREATE src/app/clock/clock.component.spec.ts (619 bytes)
CREATE src/app/clock/clock.component.ts (272 bytes)
CREATE src/app/clock/clock.component.scss (0 bytes)
UPDATE src/app/app.module.ts (739 bytes)

## Ui libraries

https://material.angular.io/components/categories
https://www.telerik.com/kendo-angular-ui
https://ng-bootstrap.github.io/#/components/accordion/examples

## Bootstrap

npm install bootstrap

"stylePreprocessorOptions": {
"includePaths": ["node_modules/bootstrap/scss/"]
}

# Playlists module

ng g m playlists -m app --routing  
ng g c playlists/containers/playlists-view
ng g c playlists/components/playlists-list
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-editor

## ngDevTools

window.ng

https://angular.io/api/core/global

```js
ng.getDirectives($0);

ng.getContext($0).selectedIdChange.emit("123");
ng.applyChanges(ng.getContext($0));
```

## Examples

ng g m examples --routing -m app
ng g c examples/dropdowns
ng g d shared/directives/dropdown --export
ng g d shared/directives/dropdown-trigger --export
ng g d shared/directives/dropdown-menu --export

```ts
const routes: Routes = [
  {
    path: "examples",
    children: [
      {
        path: "dropdowns",
        component: DropdownsComponent,
      },
    ],
  },
];
```

## Music module

ng g m music -m app --routing --route music
ng g c music/containers/album-search --type container
ng g c music/containers/album-details --type container
ng g c music/components/search-form
ng g c music/components/search-results

ng g m core -m app
ng g i core/model/Album
ng g s core/services/music-api

## Dependecy injection

https://angular.io/guide/dependency-injection

## Core vs shared

- Core
  - services

- feature/music
  - import Shared

- feature/playlists
  - import Shared

- Shared
  - export components

- App
  import Core
  import feature/*
  import Shared


## Badword API
- https://www.neutrinoapi.com/api/bad-word-filter/
- https://promptapi.com/marketplace/description/bad_words-api
- https://github.com/coldner/wulgaryzmy


## Recent search widget

ng g c shared/containers/recent-searches --export 


## Rxjs

https://rxmarbles.com/#startWith
https://rxjs.dev/operator-decision-tree
https://rxviz.com/
https://www.learnrxjs.io/learn-rxjs/subjects/behaviorsubject


## Album details container view

ng g c music/containers/album-details --type container

http://localhost:4200/music/albums?id=5Tby0U5VndHW0SomYO7Id7

getAlbumById(5Tby0U5VndHW0SomYO7Id7)

## Tracks list
ng g c music/components/track-list 

## Playlsits view + API

ng g s core/services/playlists

## # Playlists module - nested

ng g c playlists/containers/playlists-nested-view
ng g c playlists/components/routed-playlists-list
ng g c playlists/components/routed-playlist-details
ng g c playlists/components/routed-playlist-editor

ng g resolver playlists/resolvers/playlists 
ng g resolver playlists/resolvers/playlist

## Testing

https://angular.io/guide/testing
https://angular.io/guide/testing#more-information-on-testing

https://material.angular.io/cdk/test-harnesses/overview

https://github.com/ngneat/spectator

https://testing-library.com/docs/angular-testing-library/intro



